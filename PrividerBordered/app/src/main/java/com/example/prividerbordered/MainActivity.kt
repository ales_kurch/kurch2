package com.example.prividerbordered

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() { companion object {
    val APPS_URI = Uri.parse("content://com.example.kurch2.provider/apps")
    val LASTAPP_URI = Uri.parse("content://com.example.kurch2.provider/lastApp")
    const val COLUMN_PACKAGE = "packageName"
    const val COLUMN_LABEL = "lable"
    const val COLUMN_AMOUNT = "opens"
}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showPckgsBtn.setOnClickListener { requestPackages() }
        showPckgsBtn2.setOnClickListener { requestPackages2() }
    }

    private fun requestPackages(): Unit {
        try{
            val cursor = contentResolver.query(APPS_URI, null, null, null, null)
            val stringBuilder = StringBuilder().append("APPLICATIONS: \n")

            cursor?.use {
                while (it.moveToNext()) {
                    val appPackage = cursor.getString(cursor.getColumnIndex(COLUMN_PACKAGE))
                    val appLabel = cursor.getString(cursor.getColumnIndex(COLUMN_LABEL))
                    val appStarts = cursor.getLong(cursor.getColumnIndex(COLUMN_AMOUNT))


                    stringBuilder.append("---------------------\n")
                            .append("package: $appPackage\n" )
                            .append("label: $appLabel\n")
                            .append("starts amount: $appStarts\n")
                }
            }
            tv.text = stringBuilder.toString()
        } catch (_: Exception) {
            Toast.makeText(this, "app haven't access to info", Toast.LENGTH_LONG).show()
        }

    }
    private fun requestPackages2(): Unit {
        try{
            val cursor2 = contentResolver.query(LASTAPP_URI, null, null, null, null)
            val stringBuilder2 = StringBuilder().append("LAST APPLICATION: \n")
            cursor2?.use {
                while (it.moveToNext()) {
                    val appPackage = cursor2.getString(cursor2.getColumnIndex(COLUMN_PACKAGE))
                    val appLabel = cursor2.getString(cursor2.getColumnIndex(COLUMN_LABEL))
                    val appStarts = cursor2.getLong(cursor2.getColumnIndex(COLUMN_AMOUNT))


                    stringBuilder2.append("---------------------\n")
                            .append("package: $appPackage\n" )
                            .append("label: $appLabel\n")
                            .append("starts amount: $appStarts\n")
                }
            }
            tv2.text = stringBuilder2.toString()
        } catch (_: Exception) {
            Toast.makeText(this, "app haven't access to info", Toast.LENGTH_LONG).show()
        }

    }
}
