package com.example.kurch2

import android.content.ContentProvider
import android.content.ContentResolver
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.util.Log
import com.example.kurch2.DataBase.LastAppDatabase
import com.example.kurch2.DataBase.WordRoomDatabase

class MyContentProvider : ContentProvider() {

    private lateinit var appDB: WordRoomDatabase
    private lateinit var lappDB: LastAppDatabase

    companion object {
        const val AUTHORITY = "com.example.kurch2.provider"

        private const val APPS = 1
        private const val LASTAPP=2

        private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            uriMatcher.addURI(AUTHORITY, "apps", APPS)
            uriMatcher.addURI(AUTHORITY, "lastApp", LASTAPP)
        }
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return null
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        Log.d("1111", "content provider: ${uri.toString()}")
        return when (uriMatcher.match(uri)) {
            APPS -> appDB.wordDao().getUsedToCursor()
            LASTAPP-> lappDB.wordDao().getUsedToCursor()
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
    }

    override fun onCreate(): Boolean {
        try {
            appDB = WordRoomDatabase.getWordRoomDatabase(context!!)!!
            lappDB= LastAppDatabase.getLastAppDatabase(context!!)!!
            return true
        } catch (_: Exception) {
            throw Exception("Can't create database: context is null")
        }
    }

    override fun update(
        uri: Uri,
        values: ContentValues,
        selection: String?,
        selectionArgs: Array<String>?
    ): Int {

        val apps = appDB.wordDao()
        val apps2 = lappDB.wordDao()

        when (uriMatcher.match(uri)) {
            APPS -> {
                val appPackage = values.getAsString("packageName")
                val amount = values.getAsInteger("opens")

                return apps.updateAppStarts(appPackage, amount)
            }
            LASTAPP->{
                val appPackage = values.getAsString("packageName")
                val amount = values.getAsInteger("opens")
                return apps2.updateAppStarts(appPackage, amount)
            }
            else -> throw java.lang.IllegalArgumentException("Unsupported URI: $uri")
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int = 0

    override fun getType(uri: Uri): String? = when (uriMatcher.match(uri)) {
        APPS -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider.apps"
        LASTAPP -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider.lastApp"
        else -> throw IllegalArgumentException("Unsupported URI: $uri")
    }
}
