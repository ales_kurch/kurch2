package com.example.kurch2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_PACKAGE_ADDED
import android.content.Intent.ACTION_PACKAGE_REMOVED
import android.util.Log
import com.example.kurch2.DataBase.HomePageDatabase
import com.example.kurch2.DataBase.Word
import com.example.kurch2.DataBase.WordRoomDatabase
import com.example.kurch2.service.Wallpapers
import com.example.kurch2.ui.pager.walls
import com.yandex.metrica.YandexMetrica
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyReceiver : BroadcastReceiver() {
    var mDb: WordRoomDatabase? = null
    var hDb: HomePageDatabase? = null
    override fun onReceive(context: Context, intent: Intent) {
        mDb = WordRoomDatabase.getWordRoomDatabase(context!!)
        hDb = HomePageDatabase.getHomePageDatabase(context!!)
        Log.d("TAG", "ReciverEnter")
        if (intent.action == ACTION_PACKAGE_ADDED) {
            YandexMetrica.reportEvent("Install app")
            val info = context.packageManager.getApplicationInfo(intent.data.schemeSpecificPart, 0)
            var pName = info.packageName
            val pLable = info.loadLabel(context!!.packageManager).toString()
            Thread{
            mDb?.wordDao()?.insert(Word(pName, pLable, 0))}.start()
        }
        if (intent.action == ACTION_PACKAGE_REMOVED) {
            Thread{
                mDb?.wordDao()?.delete(intent.data.schemeSpecificPart)
            }.start()
        }
        if(intent.action == "com.example.kurch2.utils.action.UPDATE_WALLPAPERS")
        {
            Log.d("TAG", "ReciverWork")
            val scope = CoroutineScope(Dispatchers.Default)
            scope.launch {
                val temp= Wallpapers.getWallpaper()
                withContext(Dispatchers.Main){
                    walls.setImageDrawable(temp)
                }
            }
        }
    }
}