package com.example.kurch2

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.kurch2.DataBase.HomePageDatabase
import com.example.kurch2.DataBase.LastAppDatabase
import com.example.kurch2.DataBase.WordRoomDatabase
import com.example.kurch2.service.MyIntentService
import com.example.kurch2.service.StoryDownloadWork
import com.example.kurch2.service.Wallpapers
import com.example.kurch2.ui.AppsOnDevice
import com.example.kurch2.ui.pager.walls
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit


lateinit var pref: SharedPreferences
lateinit var mDb: WordRoomDatabase
lateinit var lDb: LastAppDatabase
lateinit var hDb: HomePageDatabase
lateinit var appList: MutableList<AppsOnDevice>
lateinit var favList: MutableList<AppsOnDevice>
var hasNotStarted = true
lateinit var receiver: BroadcastReceiver

class Menu : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        pref = PreferenceManager.getDefaultSharedPreferences(this)
        mDb = WordRoomDatabase.getWordRoomDatabase(this)!!
        lDb = LastAppDatabase.getLastAppDatabase(this)!!
        hDb = HomePageDatabase.getHomePageDatabase(this)!!
        if (pref.getBoolean("welcomePage", true) && hasNotStarted) {
            pref.edit().putBoolean("welcomePage", true).apply()
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            if (hasNotStarted) {
                finish()
                startActivity(Intent(this, SplashScreen::class.java))
            }
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        val toolbar: Toolbar = findViewById(R.id.toolbar2)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow, R.id.nav_start
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        if (pref.getBoolean("sync", false))
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }


    override fun onResume() {
        super.onResume()
        if (pref.getString("wallchange", "1") != "1" && pref.getString(
                "wallchange",
                "1"
            ) != "1221"
        ) {
            Log.d("Check", "prefGone")
            val scope = CoroutineScope(Dispatchers.Default)
            scope.launch {
                val temp= Wallpapers.getWallpaper()
                withContext(Dispatchers.Main){
                    walls.setImageDrawable(temp)
                }
            }
            val downloadWork =
                PeriodicWorkRequestBuilder<StoryDownloadWork>(
                    pref.getString(
                        "wallchange",
                        "1"
                    ).toLong(), TimeUnit.MINUTES
                ).build()
            WorkManager.getInstance()
                .enqueueUniquePeriodicWork(
                    "YOUR_TAG",
                    ExistingPeriodicWorkPolicy.KEEP,
                    downloadWork
                )
        }
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED)
        filter.addAction(Intent.ACTION_PACKAGE_ADDED)
        filter.addDataScheme("package")
        var wallpaperFilter = IntentFilter().apply {
            addAction(MyIntentService.BROADCAST_ACTION_UPDATE_WALLPAPERS)
        }
        receiver= MyReceiver()
        registerReceiver(receiver, filter)
        registerReceiver(receiver, wallpaperFilter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(receiver)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    fun onClickProfile(view: View) {
        val intent = Intent();
        intent.setClass(this, Profile::class.java)
        startActivity(intent)
    }
}
