package com.example.kurch2.service

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import com.example.kurch2.MyApp
import com.example.kurch2.R
import com.example.kurch2.pref
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

class Wallpapers {

    companion object {
        var link = pref.getString("wallSite", "https://picsum.photos/1000/2000")
        const val WALLPAPER_DIR = "wallpapers"
        const val WALLPAPER_NAME = "wallpaper"

        val scope = CoroutineScope(Dispatchers.Default)


        suspend fun getWallpaper(): BitmapDrawable =
            withContext(scope.coroutineContext) {
                var bitmap: Bitmap?
                try {
                    bitmap = Picasso.get()
                        .load(getDiscCacheFile(WALLPAPER_NAME))
                        .get()
                } catch (_: Exception) {
                    bitmap = null
                }

                if (bitmap == null) {
                    updateWallpaper().join()
                    return@withContext BitmapDrawable(
                        MyApp.appContext.resources,
                        Picasso.get()
                            .load(getDiscCacheFile(WALLPAPER_NAME))
                            .get()
                    )
                } else {
                    return@withContext BitmapDrawable(
                        MyApp.appContext.resources,
                        bitmap
                    )
                }

            }

        fun updateWallpaper() =
            scope.launch {
                updateCache(
                    Picasso.get().load(link).memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.drawable.wallpaper).get()
                ).join()
            }


        private fun updateCache(bitmap: Bitmap) = scope.launch {
            val file = getDiscCacheFile(WALLPAPER_NAME)
            val fOut = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut.close()
        }

        private fun getDiscCacheFile(fileName: String): File {
            val path = MyApp.appContext.getDir(WALLPAPER_DIR, Context.MODE_PRIVATE)
            return File(path, fileName)
        }
    }
}