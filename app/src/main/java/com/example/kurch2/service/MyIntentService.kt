package com.example.kurch2.service

import android.app.IntentService
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MIN
import com.example.kurch2.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch



class MyIntentService : IntentService("MyIntentService") {
    companion object{
        const val BROADCAST_ACTION_UPDATE_WALLPAPERS="com.example.kurch2.utils.action.UPDATE_WALLPAPERS"
    }

    override fun onCreate() {
        super.onCreate()
        val channelId =
                createNotificationChannel("my_service", "My Background Service")
        val notificationBuilder = NotificationCompat.Builder(this, channelId )
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(PRIORITY_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(101, notification)
    }

    override fun onHandleIntent(intent: Intent?) {
        CoroutineScope(Dispatchers.Default).launch {
            Log.d("TAG", "Start")
            Wallpapers.updateWallpaper().join()
            val wallpaperIntent= Intent()
            wallpaperIntent.action= BROADCAST_ACTION_UPDATE_WALLPAPERS
            sendBroadcast(wallpaperIntent)
        }


    }
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }
}
