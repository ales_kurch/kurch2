package com.example.kurch2.service

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters


class StoryDownloadWork(val context: Context, workerParams: WorkerParameters) : Worker(context,
    workerParams
) {
    override fun doWork(): Result {
        Log.d("tag", "success")
        val intentMyIntentService = Intent(context , MyIntentService::class.java)
        context.startForegroundService(intentMyIntentService)
        return Result.success()
    }

}