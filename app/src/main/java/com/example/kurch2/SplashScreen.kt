package com.example.kurch2

import android.content.Intent
import android.content.pm.ResolveInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.kurch2.DataBase.Word
import com.example.kurch2.DataBase.Word3

class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        hasNotStarted=false
        var thread1 = Thread {
            val mainIntent = Intent(Intent.ACTION_MAIN, null)
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
            val pkgAppsList: MutableList<ResolveInfo> =
                this?.packageManager?.queryIntentActivities(
                    mainIntent,
                    0
                ) as MutableList<ResolveInfo>
            if (mDb?.wordDao()?.getNotLiveWords().isEmpty()) {
                pkgAppsList.forEach { it ->
                    if(it.activityInfo.packageName!="com.example.kurch2"){
                        mDb?.wordDao()?.insert(
                            Word(
                                it.activityInfo.packageName,
                                it.loadLabel(this!!.packageManager).toString(),
                                0
                            )
                        )}
                }
            }
            if(hDb?.wordDao()?.getNotLiveWords().isEmpty()){
                hDb.wordDao().insert(Word3(
                    pkgAppsList[0].activityInfo.packageName,
                    pkgAppsList[0].loadLabel(this!!.packageManager).toString(),
                    0
                ))
                hDb.wordDao().insert(Word3(
                    pkgAppsList[1].activityInfo.packageName,
                    pkgAppsList[1].loadLabel(this!!.packageManager).toString(),
                    0
                ))
                hDb.wordDao().insert(Word3(
                    pkgAppsList[2].activityInfo.packageName,
                    pkgAppsList[2].loadLabel(this!!.packageManager).toString(),
                    0
                ))
                hDb.wordDao().insert(Word3(
                    pkgAppsList[3].activityInfo.packageName,
                    pkgAppsList[3].loadLabel(this!!.packageManager).toString(),
                    0
                ))
                hDb.wordDao().insert(Word3(
                    pkgAppsList[4].activityInfo.packageName,
                    pkgAppsList[4].loadLabel(this!!.packageManager).toString(),
                    4
                ))
                for(i in 5..19){
                    hDb.wordDao().insert(Word3(
                        "empty$i",
                        "empty$i",
                        i
                    ))
                }
            }
        }
        thread1.start()
        Handler().postDelayed({
            startActivity(Intent(this,Menu::class.java))
            finish()
        }, 3000)
    }
}
