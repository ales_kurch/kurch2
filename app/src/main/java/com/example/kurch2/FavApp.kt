package com.example.kurch2

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kurch2.DataBase.Word
import com.example.kurch2.DataBase.Word2
import com.example.kurch2.ui.AppsOnDevice
import com.example.kurch2.ui.gallery.land
import com.example.kurch2.ui.gallery.offset
import com.example.kurch2.ui.gallery.portrait
import com.yandex.metrica.YandexMetrica

class FavApp : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_fav_app, container, false)
            mDb?.wordDao()?.getOpensRangeWords()?.observe(
                this,
                Observer<List<Word>> {
                    this.renderMessges(
                        it
                    )
                })
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
            root.findViewById<RecyclerView>(R.id.recyclerviewFav).layoutManager =
                GridLayoutManager(context, portrait)
        else
            root.findViewById<RecyclerView>(R.id.recyclerviewFav).layoutManager =
                GridLayoutManager(context, land)
        return root
    }

    class Adapter(
        private val context: Context,
        private val messages: List<Word>?,
        private val icons: List<AppsOnDevice>?
    ) :
        RecyclerView.Adapter<Adapter.ViewHolder>() {
        class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
            var textView: TextView? = null
            var image: TextView? = null
            var popupMenu: PopupMenu? = null

            init {
                textView = itemView?.findViewById(R.id.textView10)
                image = itemView?.findViewById(R.id.text_list_item)
                popupMenu = PopupMenu(itemView?.context, image)
                popupMenu?.inflate(R.menu.pop_up_menu)
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView =
                LayoutInflater.from(parent?.context).inflate(R.layout.list_item_view, parent, false)
            return ViewHolder(itemView)
        }

        override fun getItemCount() = offset

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder?.image?.background = icons?.get(position)?.icon
            holder?.textView?.text = messages?.get(position)?.lable
            holder.image?.setOnClickListener {
                Thread {
                    YandexMetrica.reportEvent("Open app in Launcher")
                    var opens = messages!![position].opens + 1
                    mDb.wordDao().update(
                        Word(
                            messages!![position].packageName,
                            messages!![position].lable,
                            opens
                        )
                    )
                    lDb.clearAllTables()
                    lDb.wordDao().insert(
                        Word2(
                            messages!![position].packageName,
                            messages!![position].lable,
                            opens
                        )
                    )
                }.start()
                var intent: Intent? =
                    holder?.itemView?.context?.packageManager?.getLaunchIntentForPackage(messages!![position].packageName)
                holder?.itemView?.context?.startActivity(intent)
            }
        }
    }

    private fun renderMessges(messages: List<Word>?) {
        appList = messages?.map {
            val icon = context?.packageManager?.getApplicationIcon(it.packageName)
            AppsOnDevice(icon)
        }?.toMutableList()!!
        activity?.findViewById<RecyclerView>(R.id.recyclerviewFav)?.adapter =
            Adapter(context!!, messages, appList)
    }
}