package com.example.kurch2

import android.app.Application
import android.content.Context
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig


class MyApp: Application() {
    companion object{
        internal lateinit var appContext: Context
    }
    override fun onCreate() {
        super.onCreate()
        appContext=applicationContext
        val config = YandexMetricaConfig.newConfigBuilder("4eb7f5d0-ede5-4730-b327-0d6d5f290b4b").build()
        // Initializing the AppMetrica SDK.
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this)
    }
}