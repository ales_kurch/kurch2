package com.example.kurch2.ui.homePage

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kurch2.*
import com.example.kurch2.DataBase.HomePageDatabase
import com.example.kurch2.DataBase.LastAppDatabase
import com.example.kurch2.DataBase.Word3
import com.example.kurch2.DataBase.WordRoomDatabase
import com.example.kurch2.ui.AppsOnDevice
import com.squareup.picasso.Picasso
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.alter_dialog.*

lateinit var icon: Drawable
lateinit var alterText: String

class HomePage : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.activity_home, container, false)
        mDb = WordRoomDatabase.getWordRoomDatabase(root.context)!!
        lDb= LastAppDatabase.getLastAppDatabase(root.context)!!
        hDb= HomePageDatabase.getHomePageDatabase(root.context)!!
        hDb?.wordDao()?.getWords()?.observe(this, Observer<List<Word3>> { this.renderMessges(it) })


        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
            root.findViewById<RecyclerView>(R.id.recyclerviewHome).layoutManager =
                GridLayoutManager(context, 5)
        else
            root.findViewById<RecyclerView>(R.id.recyclerviewHome).layoutManager =
                GridLayoutManager(context, 8)
        root.findViewById<TextView>(R.id.addLink).setOnClickListener {
            val editAlter = AlertDialog.Builder(root.context).create()
            val editView = layoutInflater.inflate(R.layout.alter_dialog, null)
            editAlter.setView(editView)
            editAlter.setButton(AlertDialog.BUTTON_POSITIVE, "Apply") { _, _ ->
                alterText = editAlter.textAlterDialog.text.toString()
                if (URLUtil.isValidUrl(alterText)) {
                    var hasEnothPlace = true
                    var counter = 0
                    var thread1 = Thread {
                        var listWord3 = hDb.wordDao().getNotLiveWords()
                        for (i in 0..19) {
                            if (listWord3[i].lable == alterText) {
                                counter = 1
                                hasEnothPlace = false
                                break
                            }
                        }
                        if (counter == 0) {
                            for (i in 0..19) {
                                if (listWord3[i].lable == "empty${listWord3[i].opens}") {
                                    hDb?.wordDao()?.insert(
                                        Word3(
                                            "url${listWord3[i].opens}",
                                            alterText,
                                            listWord3[i].opens
                                        )
                                    )
                                    hasEnothPlace = false
                                    break
                                }
                            }
                        }
                    }
                    thread1.start()
                    thread1.join()
                    if (hasEnothPlace)
                        Toast.makeText(
                            root.context,
                            "It's too much apps on home page",
                            Toast.LENGTH_LONG
                        ).show()
                } else
                    Toast.makeText(
                        root.context,
                        "Your URL is invalid",
                        Toast.LENGTH_LONG
                    ).show()
            }
            editAlter.setOnShowListener {
                editAlter.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(resources.getColor(R.color.colorAccentLight))
            }
            editAlter.show()
        }
        return root
    }

    class Adapter(
        private val context: Context,
        private val messages: List<Word3>,
        private val icons: List<AppsOnDevice>?
    ) :
        RecyclerView.Adapter<Adapter.ViewHolder>() {
        class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
            var textView: TextView? = null
            var image: ImageView? = null
            var popupMenu: PopupMenu? = null
            var popupMenuUrl: PopupMenu? = null

            init {
                textView = itemView?.findViewById(R.id.text_hpme)
                image = itemView?.findViewById(R.id.img_home)
                popupMenu = PopupMenu(itemView?.context, image)
                popupMenu?.inflate(R.menu.pop_up_menu2)
                popupMenuUrl = PopupMenu(itemView?.context, image)
                popupMenuUrl?.inflate(R.menu.pop_up_menu3)
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView =
                LayoutInflater.from(parent?.context).inflate(R.layout.item_home_page, parent, false)
            return ViewHolder(itemView)
        }

        override fun getItemCount() = messages!!.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (messages[position].lable != "empty$position") {
                if (messages[position].packageName != "url$position") {
                    holder?.image?.setImageDrawable(icons?.get(position)?.icon)
                    holder?.textView?.text = messages?.get(position)?.lable
                } else {
                    var result =
                        messages[position].lable.removePrefix("http://").removePrefix("https://")
                            .removePrefix("www.")
                    Picasso.get()
                        .load("https://i.olsh.me/icon?size=120&url=" + messages[position].lable)
                        .placeholder(R.drawable.ic_filter_vintage_black_24dp)
                        .error(R.drawable.ic_sentiment_very_dissatisfied_black_24dp)
                        .into(holder?.image)
                    holder?.textView?.text = result
                }
            } else
                holder?.itemView?.visibility = View.GONE
            holder.image?.setOnClickListener {
                if (messages[position].packageName != "url$position") {
                    var intent: Intent? =
                        holder?.itemView?.context?.packageManager?.getLaunchIntentForPackage(
                            messages!![position].packageName
                        )
                    holder?.itemView?.context?.startActivity(intent)
                } else {
                    val packageUri: Uri =
                        Uri.parse(messages[position].lable)
                    val uninstallIntent =
                        Intent(Intent.ACTION_VIEW, packageUri)
                    holder?.itemView?.context?.startActivity(uninstallIntent)
                }
            }
            holder.image?.setOnLongClickListener {
                if (messages[position].packageName != "url$position") {

                    holder?.popupMenu?.show()
                    holder?.popupMenu?.setOnMenuItemClickListener { item: MenuItem? ->

                        when (item!!.itemId) {
                            R.id.one -> {
                                var thread1 = Thread {
                                    hDb?.wordDao()?.insert(
                                        Word3(
                                            "empty${messages!![position].opens}",
                                            "empty${messages!![position].opens}",
                                            messages!![position].opens
                                        )
                                    )
                                }
                                thread1.start()
                                thread1.join()
                                val packageUri: Uri =
                                    Uri.parse("package:" + messages!![position].packageName)
                                val uninstallIntent =
                                    Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri)
                                holder?.itemView?.context?.startActivity(uninstallIntent)
                            }
                            R.id.four -> {
                                var thread1 = Thread {
                                    hDb?.wordDao()?.insert(
                                        Word3(
                                            "empty${messages!![position].opens}",
                                            "empty${messages!![position].opens}",
                                            messages!![position].opens
                                        )
                                    )
                                }
                                thread1.start()
                                thread1.join()
                            }
                            R.id.three -> {
                                YandexMetrica.reportEvent("Open System info in Launcher")
                                val i = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                i.addCategory(Intent.CATEGORY_DEFAULT)
                                i.data = Uri.parse("package:" + messages!![position].packageName)
                                holder?.itemView?.context?.startActivity(i)
                            }
                        }

                        true
                    }
                }
                else{
                    holder?.popupMenuUrl?.show()
                    holder?.popupMenuUrl?.setOnMenuItemClickListener { item: MenuItem? ->

                        when (item!!.itemId) {
                            R.id.four -> {
                                var thread1 = Thread {
                                    hDb?.wordDao()?.insert(
                                        Word3(
                                            "empty${messages!![position].opens}",
                                            "empty${messages!![position].opens}",
                                            messages!![position].opens
                                        )
                                    )
                                }
                                thread1.start()
                                thread1.join()
                            }
                        }
                        true
                    }
                }
                true
            }
        }
    }


    override fun onResume() {
        super.onResume()
    }

    private fun renderMessges(messages: List<Word3>) {
        var sourcePosition = -1
        var targetPosition = -1
        counter = 0
        appList = messages?.map {
            icon = if (it.lable != "empty$counter") {
                if (it.packageName == "url${it.opens}") {
                    context?.packageManager?.getApplicationIcon("com.example.kurch2")!!
                } else
                    context?.packageManager?.getApplicationIcon(it.packageName)!!
            } else
                context?.packageManager?.getApplicationIcon("com.example.kurch2")!!
            counter++
            AppsOnDevice(icon)
        }?.toMutableList()!!
        var adapter = Adapter(context!!, messages, appList)
        activity?.findViewById<RecyclerView>(R.id.recyclerviewHome)?.adapter = adapter
    }
}
