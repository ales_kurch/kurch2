package com.example.kurch2.ui.slideshow

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceFragmentCompat
import com.example.kurch2.R
import com.example.kurch2.pref
import com.yandex.metrica.YandexMetrica

class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)

    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        if(p1=="sync"){
            if(pref.getBoolean("sync", false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
            else
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        if(p1=="wall"){
            pref.getString("wall", "varvara")
            when(pref.getString("wall", "varvara")) {
                "olga" ->{
                    YandexMetrica.reportEvent("Selected wallpaper Olga")}
                "varvara" -> {YandexMetrica.reportEvent("Selected wallpaper Varvara")}
                "yana" -> {YandexMetrica.reportEvent("Selected wallpaper Yana")}
                "elise" -> {YandexMetrica.reportEvent("Selected wallpaper Elise")}
                "alice" -> {YandexMetrica.reportEvent("Selected wallpaper Alice")}
                "poline" -> {YandexMetrica.reportEvent("Selected wallpaper Poline")}
            }
        }
    }
}
