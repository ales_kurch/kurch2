package com.example.kurch2.ui.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.kurch2.R
import com.example.kurch2.pref
import com.example.kurch2.service.Wallpapers
import com.example.kurch2.ui.gallery.LauncherFragment
import com.example.kurch2.ui.homePage.HomePage
import com.example.kurch2.ui.list.ListActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

lateinit var walls: ImageView

class ViewPagerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.activity_view_pager_fragment, container, false)
        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(HomePage(), "Home")
        adapter.addFragment(LauncherFragment(), "Launcher")
        adapter.addFragment(ListActivity(), "List")
        var viewPager = root.findViewById<ViewPager>(R.id.viewPager)
        viewPager.adapter = adapter
        walls = root.findViewById(R.id.wallPapper)
        if (pref.getString("wallchange", "1") == "1221") {
            val scope = CoroutineScope(Dispatchers.Default)
            scope.launch {
                val temp = Wallpapers.getWallpaper()
                withContext(Dispatchers.Main) {
                    walls.setImageDrawable(temp)
                }
            }
        }
        return root
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        val fragmentList: MutableList<Fragment> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return 3
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
        }
    }

    override fun onResume() {
        super.onResume()
        if (pref.getString("wallchange", "1") == "1") {
            when (pref.getString("wall", "varvara")) {
                "olga" -> walls.setImageResource(R.drawable.olga)
                "varvara" -> walls.setImageResource(R.drawable.varvara)
                "yana" -> walls.setImageResource(R.drawable.yana)
                "elise" -> walls.setImageResource(R.drawable.elise)
                "alice" -> walls.setImageResource(R.drawable.alice)
                "poline" -> walls.setImageResource(R.drawable.poline)
            }
        } else {
            val scope = CoroutineScope(Dispatchers.Default)
            scope.launch {
                val temp = Wallpapers.getWallpaper()
                withContext(Dispatchers.Main) {
                    walls.setImageDrawable(temp)
                }
            }
        }
    }

}
