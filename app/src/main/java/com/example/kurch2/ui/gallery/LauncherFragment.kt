package com.example.kurch2.ui.gallery


import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kurch2.*
import com.example.kurch2.DataBase.Word
import com.example.kurch2.DataBase.Word2
import com.example.kurch2.DataBase.Word3
import com.example.kurch2.ui.AppsOnDevice
import com.yandex.metrica.YandexMetrica

var land = 6
var portrait = 4
var offset=4

class LauncherFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.recycler_view, container, false)
        if (pref.getString("list", "charlie") == "alpha")
            mDb?.wordDao()?.getAlphabetizedWords()?.observe(
                this,
                Observer<List<Word>> {
                    this.renderMessges(
                        it
                    )
                })
        if (pref.getString("list", "charlie") == "charlie")
            mDb?.wordDao()?.getWords()?.observe(
                this,
                Observer<List<Word>> {
                    this.renderMessges(
                        it
                    )
                })
        if (pref.getString("list", "charlie") == "gamma")
            mDb?.wordDao()?.getOpensRangeWords()?.observe(
                this,
                Observer<List<Word>> {
                    this.renderMessges(
                        it
                    )
                })
        if (pref.getString("list", "charlie") == "beta")
            mDb?.wordDao()?.getAlphabetizedReverseWords()?.observe(
                this,
                Observer<List<Word>> {
                    this.renderMessges(
                        it
                    )
                })
        if (pref.getBoolean("attachment", false)) {
            land = 7
            portrait = 5
        } else {
            land = 6
            portrait = 4
        }

        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            root.findViewById<RecyclerView>(R.id.recyclerview).layoutManager =
                GridLayoutManager(context, portrait)
            offset= portrait
        }
        else{
            root.findViewById<RecyclerView>(R.id.recyclerview).layoutManager =
                GridLayoutManager(context, land)
            offset= land
        }
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerFav, FavApp()).commit()
        return root
    }

    class Adapter(
        private val context: Context,
        private val messages: List<Word>?,
        private val icons: List<AppsOnDevice>?
        ) :
        RecyclerView.Adapter<Adapter.ViewHolder>() {
        class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
            var textView: TextView? = null
            var image: TextView? = null
            var popupMenu: PopupMenu? = null

            init {
                textView = itemView?.findViewById(R.id.textView10)
                image = itemView?.findViewById(R.id.text_list_item)
                popupMenu = PopupMenu(itemView?.context, image)
                popupMenu?.inflate(R.menu.pop_up_menu)
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView =
                LayoutInflater.from(parent?.context).inflate(R.layout.list_item_view, parent, false)
            return ViewHolder(itemView)
        }

        override fun getItemCount() = messages!!.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder?.image?.background = icons?.get(position)?.icon
            holder?.textView?.text = messages?.get(position)?.lable
            holder.image?.setOnClickListener {
                Thread {
                    YandexMetrica.reportEvent("Open app in Launcher")
                    var opens = messages!![position].opens + 1
                    mDb.wordDao().update(
                        Word(
                            messages!![position].packageName,
                            messages!![position].lable,
                            opens
                        )
                    )
                    lDb.clearAllTables()
                    lDb.wordDao().insert(
                        Word2(
                            messages!![position].packageName,
                            messages!![position].lable,
                            opens
                        )
                    )
                }.start()
                var intent: Intent? =
                    holder?.itemView?.context?.packageManager?.getLaunchIntentForPackage(messages!![position].packageName)
                holder?.itemView?.context?.startActivity(intent)
            }
            holder.image?.setOnLongClickListener {
                holder?.popupMenu?.show()
                holder?.popupMenu?.setOnMenuItemClickListener { item: MenuItem? ->

                    when (item!!.itemId) {
                        R.id.one -> {
                            YandexMetrica.reportEvent("Delete app in Launcher")
                            var thread1 = Thread {
                                var listWord3 = hDb.wordDao().getNotLiveWords()
                                for (i in 0..19) {
                                    if (messages!![position].packageName == listWord3[i].packageName) {
                                        hDb?.wordDao()?.insert(
                                            Word3(
                                                "empty${listWord3[i].opens}",
                                                "empty${listWord3[i].opens}",
                                                listWord3[i].opens
                                            )
                                        )
                                        break
                                    }
                                }
                            }
                            thread1.start()
                            thread1.join()
                            val packageUri: Uri =
                                Uri.parse("package:" + messages!![position].packageName)
                            val uninstallIntent =
                                Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri)
                            holder?.itemView?.context?.startActivity(uninstallIntent)
                        }
                        R.id.two -> {
                            YandexMetrica.reportEvent("Ask of opens in Launcher")
                            Toast.makeText(
                                holder?.itemView?.context,
                                (messages!![position].opens).toString(),
                                Toast.LENGTH_SHORT
                            ).show();
                        }
                        R.id.three -> {
                            YandexMetrica.reportEvent("Open System info in Launcher")
                            val i = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            i.addCategory(Intent.CATEGORY_DEFAULT)
                            i.data = Uri.parse("package:" + messages!![position].packageName)
                            holder?.itemView?.context?.startActivity(i)
                        }
                        R.id.four -> {
                            var hasEnothPlace = true
                            var counter = 0
                            var thread1 = Thread {
                                var listWord3 = hDb.wordDao().getNotLiveWords()
                                for (i in 0..19) {
                                    if (listWord3[i].lable == messages!![position].lable) {
                                        counter = 1
                                        hasEnothPlace = false
                                        break
                                    }
                                }
                                if (counter == 0) {
                                    for (i in 0..19) {
                                        if (listWord3[i].lable == "empty${listWord3[i].opens}") {
                                            hDb?.wordDao()?.insert(
                                                Word3(
                                                    messages!![position].packageName,
                                                    messages!![position].lable,
                                                    listWord3[i].opens
                                                )
                                            )
                                            hasEnothPlace = false
                                            break
                                        }
                                    }
                                }
                            }
                            thread1.start()
                            thread1.join()
                            if (hasEnothPlace)
                                Toast.makeText(
                                    holder?.itemView?.context,
                                    "It's too much apps on home page",
                                    Toast.LENGTH_LONG
                                ).show()
                        }
                    }

                    true
                }
                true
            }
        }
    }

    private fun renderMessges(messages: List<Word>?) {
        appList = messages?.map {
            val icon = context?.packageManager?.getApplicationIcon(it.packageName)
            AppsOnDevice(icon)
        }?.toMutableList()!!
        activity?.findViewById<RecyclerView>(R.id.recyclerview)?.adapter =
            Adapter(context!!, messages, appList)
    }

}