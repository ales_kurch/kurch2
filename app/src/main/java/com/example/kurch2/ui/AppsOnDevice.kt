package com.example.kurch2.ui

import android.graphics.drawable.Drawable

data class AppsOnDevice(
                        var icon: Drawable?)