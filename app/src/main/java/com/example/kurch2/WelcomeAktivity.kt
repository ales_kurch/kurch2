package com.example.kurch2

import android.app.UiModeManager.MODE_NIGHT_YES
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ResolveInfo
import android.content.res.Configuration.*
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.example.kurch2.DataBase.LastAppDatabase
import com.example.kurch2.DataBase.Word
import com.example.kurch2.DataBase.Word3
import com.example.kurch2.DataBase.WordRoomDatabase
import kotlinx.android.synthetic.main.fragment3_fragment.*
import kotlinx.android.synthetic.main.welcome.*


var counter: Int = 1
var currentFragment: Fragment = fragment1()
lateinit var dot: CardView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.getDefaultNightMode())
        super.onCreate(savedInstanceState)
        setContentView(R.layout.welcome)
        if(pref.getBoolean("welcomePage", true))
        {
            var thread1 = Thread {
                val mainIntent = Intent(Intent.ACTION_MAIN, null)
                mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                val pkgAppsList: MutableList<ResolveInfo> =
                    this?.packageManager?.queryIntentActivities(
                        mainIntent,
                        0
                    ) as MutableList<ResolveInfo>
                if (mDb?.wordDao()?.getNotLiveWords().isEmpty()) {
                    pkgAppsList.forEach { it ->
                        if(it.activityInfo.packageName!="com.example.kurch2"){
                            mDb?.wordDao()?.insert(
                                Word(
                                    it.activityInfo.packageName,
                                    it.loadLabel(this!!.packageManager).toString(),
                                    0
                                )
                            )}
                    }
                }
                if(hDb?.wordDao()?.getNotLiveWords().isEmpty()){
                    hDb.wordDao().insert(
                        Word3(
                        pkgAppsList[0].activityInfo.packageName,
                        pkgAppsList[0].loadLabel(this!!.packageManager).toString(),
                        0
                    )
                    )
                    hDb.wordDao().insert(Word3(
                        pkgAppsList[1].activityInfo.packageName,
                        pkgAppsList[1].loadLabel(this!!.packageManager).toString(),
                        1
                    ))
                    hDb.wordDao().insert(Word3(
                        pkgAppsList[2].activityInfo.packageName,
                        pkgAppsList[2].loadLabel(this!!.packageManager).toString(),
                        2
                    ))
                    hDb.wordDao().insert(Word3(
                        pkgAppsList[3].activityInfo.packageName,
                        pkgAppsList[3].loadLabel(this!!.packageManager).toString(),
                        3
                    ))
                    hDb.wordDao().insert(Word3(
                        pkgAppsList[4].activityInfo.packageName,
                        pkgAppsList[4].loadLabel(this!!.packageManager).toString(),
                        4
                    ))
                    for(i in 5..19){
                        hDb.wordDao().insert(Word3(
                            "empty$i",
                            "empty$i",
                            i
                        ))
                    }
                }
            }
            thread1.start()
        }
        when (counter) {
            1 -> dot = findViewById(R.id.dot1)
            2 -> dot = findViewById(R.id.dot2)
            3 -> dot = findViewById(R.id.dot3)
            4 -> dot = findViewById(R.id.dot4)
        }
        dot.setCardBackgroundColor(Color.parseColor("#42A5F5"))
        replaceFragment(currentFragment)
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event)
    }

    fun onClick(view: View) {
        change()
    }

    private fun replaceFragment(fragment: Fragment) {
        currentFragment = fragment
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, fragment).commit()
    }

    private fun newAct() {
        pref.edit().putBoolean("", false).apply()
        hasNotStarted=false
        val intent = Intent()
        intent.setClass(this, Menu::class.java)
        startActivity(intent)
    }

    fun change() {
        when (counter) {
            1 -> {
                dot = findViewById(R.id.dot2)
                dot.setCardBackgroundColor(Color.parseColor("#42A5F5"))
                dot = findViewById(R.id.dot1)
                dot.setCardBackgroundColor(Color.parseColor("#717171"))
                replaceFragment(fragment2())
            }
            2 -> {
                dot = findViewById(R.id.dot3)
                dot.setCardBackgroundColor(Color.parseColor("#42A5F5"))
                dot = findViewById(R.id.dot2)
                dot.setCardBackgroundColor(Color.parseColor("#717171"))
                replaceFragment(fragment3())
            }
            3 -> {
                dot = findViewById(R.id.dot4)
                dot.setCardBackgroundColor(Color.parseColor("#42A5F5"))
                dot = findViewById(R.id.dot3)
                dot.setCardBackgroundColor(Color.parseColor("#717171"))
                replaceFragment(fragment4())
            }
            else -> newAct()
        }
        counter++
    }
}

