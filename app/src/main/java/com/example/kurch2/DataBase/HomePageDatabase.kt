package com.example.kurch2.DataBase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Word3::class], version = 1)
abstract class HomePageDatabase : RoomDatabase() {
    abstract fun wordDao(): WordDao3
    companion object {
        var INSTANCE: HomePageDatabase? = null

        fun getHomePageDatabase(context: Context): HomePageDatabase? {
            if (INSTANCE == null){
                synchronized(WordDao3::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, HomePageDatabase::class.java, "word_table3").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}