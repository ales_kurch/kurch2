package com.example.kurch2.DataBase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Word2::class], version = 1)
abstract class LastAppDatabase : RoomDatabase() {
    abstract fun wordDao(): WordDao2
    companion object {
        var INSTANCE: LastAppDatabase? = null

        fun getLastAppDatabase(context: Context): LastAppDatabase? {
            if (INSTANCE == null){
                synchronized(WordDao2::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, LastAppDatabase::class.java, "word_table2").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}