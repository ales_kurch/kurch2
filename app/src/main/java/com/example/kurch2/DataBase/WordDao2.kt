package com.example.kurch2.DataBase

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface WordDao2 {

    @Query("SELECT * from word_table2 WHERE opens>0 ORDER BY opens DESC")
    fun getUsedToCursor(): Cursor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(word: Word2)

    @Query("UPDATE word_table2 SET opens = :amountStarts WHERE packageName = :appPackage")
    fun updateAppStarts(appPackage: String, amountStarts: Int): Int
}