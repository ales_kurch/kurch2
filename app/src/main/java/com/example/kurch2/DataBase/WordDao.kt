package com.example.kurch2.DataBase

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface WordDao {
    @Query("SELECT * from word_table ORDER BY lable ASC")
    fun getAlphabetizedWords(): LiveData<List<Word>>

    @Query("SELECT * from word_table ORDER BY lable DESC")
    fun getAlphabetizedReverseWords(): LiveData<List<Word>>

    @Query("SELECT * from word_table ORDER BY opens DESC")
    fun getOpensRangeWords(): LiveData<List<Word>>

    @Query("SELECT * from word_table")
    fun getWords(): LiveData<List<Word>>

    @Query("SELECT * from word_table")
    fun getNotLiveWords(): List<Word>

    @Query("SELECT * from word_table ORDER BY opens DESC")
    fun getNotLiveOpensRangeWords(): List<Word>

    @Query("SELECT * from word_table WHERE opens>0 ORDER BY opens DESC")
    fun getUsedToCursor(): Cursor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(word: Word)

    @Update
    fun update(word: Word)

    @Query("UPDATE word_table SET opens = :amountStarts WHERE packageName = :appPackage")
    fun updateAppStarts(appPackage: String, amountStarts: Int): Int


    @Query("DELETE FROM word_table WHERE packageName=:packagename ")
    fun delete(packagename: String)

    @Query("DELETE FROM word_table")
    fun deleteAll()
}