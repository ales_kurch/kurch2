package com.example.kurch2.DataBase

import android.content.pm.ActivityInfo
import android.graphics.drawable.Drawable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "word_table2")

class Word2(
    @PrimaryKey
    @ColumnInfo(name = "packageName") val packageName: String,
    @ColumnInfo(name = "lable") val lable: String,
    @ColumnInfo(name = "opens") val opens: Int
)