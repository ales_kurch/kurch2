package com.example.kurch2.DataBase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Word::class], version = 1)
abstract class WordRoomDatabase : RoomDatabase() {
    abstract fun wordDao(): WordDao
    companion object {
        var INSTANCE: WordRoomDatabase? = null

        fun getWordRoomDatabase(context: Context): WordRoomDatabase? {
            if (INSTANCE == null){
                synchronized(WordDao::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, WordRoomDatabase::class.java, "word_table").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}