package com.example.kurch2.DataBase

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface WordDao3 {

    @Query("SELECT * from word_table3 ORDER BY opens ASC")
    fun getWords(): LiveData<List<Word3>>

    @Query("SELECT * from word_table3 WHERE packageName=:packagename")
    fun getWord(packagename: String): Word3

    @Query("SELECT * from word_table3")
    fun getNotLiveWords(): List<Word3>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(word: Word3)

    @Update
    fun update(word: Word3)

    @Query("UPDATE word_table3 SET opens = :amountStarts WHERE packageName = :appPackage")
    fun updateApp(appPackage: String, amountStarts: Int): Int

    @Query("DELETE FROM word_table3 WHERE lable=:lable")
    fun delete(lable: String)

    @Query("DELETE FROM word_table3")
    fun deleteAll()

    @Query("SELECT COUNT(opens) FROM word_table3")
    fun getDataCount(): Int
}